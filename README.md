## Development

Ensure that both pipenv and docker are installed and available.
Set up development toolkit:

    pipenv install

Create docker development environment:

    make build

Install application in development environment:

    make install

Create database tables:

    make db

Start docker container:

    make start

Start the application

    make server

The application will be accessible on localhost:5000.
You can register new users through the application.
You can also create administrator users:

    make create-admin

Administrator users can access the admin panel at localhost:5000/admin

You can stop the docker container with:

    make stop