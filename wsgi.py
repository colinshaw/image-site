#!/usr/bin/env python3
'''
File: wsgi.py
Authors:
    2014-11-14 - C.Shaw <shaw.colin@gmail.com>
Description:
    Application entry point.
'''
import os
from datetime import datetime

import click
from pytz import utc

from app import app, db, bc
from app.roles import activeRole, verifiedRole, adminRole
from app.model import User

# CLI commands.


@app.cli.command()
def create_all_tables():
    from app.roles import active_role, verified_role, admin_role
    db.create_all()
    db.session.add_all([active_role, verified_role, admin_role])
    db.session.commit()


@app.cli.command()
def drop_all_tables():
    db.drop_all()


@app.cli.command()
@click.option('-e', '--email', prompt="Enter email")
@click.option('-u', '--username', prompt="Enter user name")
@click.password_option()
def create_admin(email, username, password):
    user = User(
        email=email,
        password=bc.generate_password_hash(
            password, rounds=12).decode('utf-8'),
        user_name=username,
    )
    user.confirmed_at = datetime.now(tz=utc)
    user.roles = [
        activeRole(),
        verifiedRole(),
        adminRole()
    ]
    db.session.add(user)
    db.session.flush()
    path = os.path.join(app.config['UPLOAD_FOLDER'], str(user.id))
    os.mkdir(path)
    os.chmod(path, mode=0o777)
    db.session.commit()
