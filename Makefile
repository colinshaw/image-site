# Build development image.
build:
	pipenv run ansible-playbook ./etc/ansible/build.yml -i ./etc/ansible/hosts
# Create Python virtualenv.
install:
	pipenv run ansible-playbook ./etc/ansible/install.yml -i ./etc/ansible/hosts
# Create development container, run continuously.
start:
	pipenv run ansible-playbook ./etc/ansible/start.yml -i ./etc/ansible/hosts
# Stop development container.
stop:
	pipenv run ansible-playbook ./etc/ansible/stop.yml -i ./etc/ansible/hosts
# Start interactive shell in running container.
shell:
	docker exec -it -w /opt/image-site image-site /bin/bash
# Start interactive shell in running container.
python-shell:
	docker exec -it -w /opt/image-site image-site /bin/bash -c "export FLASK_ENV=development && ./venv/bin/flask shell"
# Start development server.
server:
	docker exec -it -w /opt/image-site image-site /bin/bash -c "export FLASK_ENV=development && ./venv/bin/flask run --host=0.0.0.0"
create-all-tables:
	docker exec -it -w /opt/image-site image-site /bin/bash -c "export FLASK_ENV=development && ./venv/bin/flask create-all-tables"
drop-all-tables:
	docker exec -it -w /opt/image-site image-site /bin/bash -c "export FLASK_ENV=development && ./venv/bin/flask drop-all-tables"
db: drop-all-tables create-all-tables
create-admin:
	docker exec -it -w /opt/image-site image-site /bin/bash -c "export FLASK_ENV=development && ./venv/bin/flask create-admin"
# Start Node development server.
test:
	docker exec -it -w /opt/image-site image-site npm test
# Cleanup Docker environment.
prune:
	docker system prune -af
dev:
	# TODO: set-up dev pipenv virtualenv
	# TODO: set up
