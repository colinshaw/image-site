"""
File: __init__.py
Authors:
    2014-11-14 - C.Shaw <shaw.colin@gmail.com>
Description:
    Initialization module for object tracker application.
    Order of initializations and imports is particular.
"""
import os

from datetime import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_mail import Mail
from flask_login import LoginManager
from flask_admin import Admin

# Initialize the application.
app = Flask(__name__)
# Add now to jinja globals for dynamic year footer.
app.jinja_env.globals.update(now=datetime.now())

# Load configuration.
app.config.update(dict(
    NAME='Image Site',  # Name of the site.
    SERVER_NAME='localhost:5000',
    UPLOAD_FOLDER=os.path.join(os.getcwd(), 'app/static/uploads'),
    # SQLAlchemy database connection address.
    SQLALCHEMY_DATABASE_URI=(
        'postgresql://postgres:@postgres:5432/image_site'),
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    TESTING=True,

    SECRET_KEY="In development.",

    # Mail settings.
    MAIL_SERVER="",
    MAIL_PORT=465,
    MAIL_USE_TLS=False,
    MAIL_USE_SSL=True,
    MAIL_USERNAME="noreply@site.com",
    MAIL_PASSWORD="",

    ADMIN_EMAIL="administrator@site.com",

    # Set of allowed file extensions.
    EXTENSIONS=set(["png", "jpg", "jpeg", "gif"]),

    # Upload settings.
    MAX_CONTENT_LENGTH=5 * 1024 * 1024  # 5MB
))

# Ensure that upload folder exists as configured.
if not os.path.exists(app.config['UPLOAD_FOLDER']):
    os.makedirs(app.config['UPLOAD_FOLDER'])

# Bcrypt object initialization.
bc = Bcrypt(app)

# Mail object initialization.
mail = Mail(app)

# SQLAlchemy object initialization.
db = SQLAlchemy(app)

# Model class imports.
from .model import User, Role, Post, Picture

# Login object initialization.
lm = LoginManager()
lm.init_app(app)


# User loader callback for LoginManager.
@lm.user_loader
def loadUser(user_id):
    """
    Returns User object from database.
    """
    return User.query.get(user_id)


# Principal and role based access control import.
from . import roles


# Controller import.
from . import controller

# Admin view import.
from .admin import (
    HomeView, UsersView, RolesView, PostsView, PicturesView, FileView)

# Admin object initialization.
admin = Admin(app, index_view=HomeView(), name=app.config['NAME'])

admin.add_view(UsersView(User, db.session, name='Users'))
admin.add_view(RolesView(Role, db.session, name='Roles'))
admin.add_view(PostsView(Post, db.session, name='Posts'))
admin.add_view(PicturesView(Picture, db.session, name='Pictures'))
admin.add_view(
    FileView(
        app.config["UPLOAD_FOLDER"], '/static/uploads/', name="Uploaded Files"
    )
)
