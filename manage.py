#!/usr/bin/env python3
"""
Name: manage.py
Authors:
    2014-11-14 - C.Shaw <shaw.colin@gmail.com>
Description:
    Script to manage database migrations.
"""

from app import app, db, model
from flask_migrate import Migrate, MigrateCommand

# Migrate object initialization.
migrate = Migrate(app, db)
